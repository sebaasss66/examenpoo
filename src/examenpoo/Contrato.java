/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo;

/**
 *
 * @author sebas
 */
public class Contrato {
    private int claveContrato;
    private String puesto;
    private float impuestoISR;

    public Contrato() {
        this.claveContrato = 0;
        this.puesto = "";
        this.impuestoISR = 0.0f;
    }

    public Contrato(int claveContrato, String puesto, float impuestoISR) {
        this.claveContrato = claveContrato;
        this.puesto = puesto;
        this.impuestoISR = impuestoISR;
    }


    public int getClaveContrato() {
        return claveContrato;
    }

    public void setClaveContrato(int claveContrato) {
        this.claveContrato = claveContrato;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoISR() {
        return impuestoISR;
    }

    public void setImpuestoISR(float impuestoISR) {
        this.impuestoISR = impuestoISR;
    }
    
    
}

    
    


    
    

