/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo;

/**
 *
 * @author sebas
 */
public class EmpleadoDocente extends Empleados {
    private int nivel;
    private float horasLaborales;
    private float pagoHoras;

    public EmpleadoDocente(int nivel, float horasLaborales, float pagoHoras, int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLaborales = horasLaborales;
        this.pagoHoras = pagoHoras;
    }


    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHorasLaborales() {
        return horasLaborales;
    }

    public void setHorasLaborales(float horasLaborales) {
        this.horasLaborales = horasLaborales;
    }

    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    @Override
    public float calcularTotal() {
        float pagoBasico = horasLaborales * pagoHoras;
        float pagoAdicional = 0;

        switch (nivel) {
            case 1:
                pagoAdicional = pagoBasico * 0.35f;
                break;
            case 2:
                pagoAdicional = pagoBasico * 0.40f;
                break;
            case 3:
                pagoAdicional = pagoBasico * 0.50f;
                break;
        }

        return pagoBasico + pagoAdicional;
    }

    @Override
    public float calcularImpuesto() {
        float total = calcularTotal();
        return total * contrato.getImpuestoISR();
    }
    
}
    

