/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo;

/**
 *
 * @author sebas
 */
public abstract class Empleados {
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected Contrato contrato;

    public Empleados() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
        this.contrato = null;
    }

    public Empleados(int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.contrato = contrato;
    }


    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public abstract float calcularTotal();

    public abstract float calcularImpuesto();
}