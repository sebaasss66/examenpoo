
package examenpoo;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */

public class jintContrato extends javax.swing.JDialog {

    private EmpleadoDocente docente;
    private Contrato contrato;
    
    /**
     * Creates new form jintContrato
     */
    public jintContrato(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.deshabilitar();
    }

     public void deshabilitar(){
        this.txtClaveContrato.setEnabled(false);
        this.txtNumEmpleado.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtPuesto.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.cmbNivel.setEnabled(false);
        this.txtPagoHora.setEnabled(false);
        this.txtHorasTrabajadas.setEnabled(false);
        
        this.txtTotal.setEnabled(false);
        this.txtPagoAdicional.setEnabled(false);
        this.txtPagoImpuesto.setEnabled(false);
        this.txtPagoTotal.setEnabled(false);
        
        this.btmGuardar.setEnabled(false);
        this.btmMostrar.setEnabled(false);
        
    }
    public void habilitar(){
        this.txtClaveContrato.setEnabled(!false);
        this.txtNumEmpleado.setEnabled(!false);
        this.txtNombre.setEnabled(!false);
        this.txtDomicilio.setEnabled(!false);
        this.txtPuesto.setEnabled(!false);
        this.txtImpuesto.setEnabled(!false);
        this.cmbNivel.setEnabled(!false);
        this.txtPagoHora.setEnabled(!false);
        this.txtHorasTrabajadas.setEnabled(!false);
        
        this.btmGuardar.setEnabled(true);
        this.btmMostrar.setEnabled(true);
        
    }
    public void limpiar(){
        this.txtClaveContrato.setText("");
        this.txtNumEmpleado.setText("");
        this.txtNombre.setText("");
        this.txtDomicilio.setText("");
        this.txtPuesto.setText("");
        this.txtImpuesto.setText("");
        this.cmbNivel.setSelectedIndex(0);
        this.txtPagoHora.setText("");
        this.txtHorasTrabajadas.setText("");
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtClaveContrato = new javax.swing.JTextField();
        txtNumEmpleado = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtPagoHora = new javax.swing.JTextField();
        txtHorasTrabajadas = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        btmNuevo = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmMostrar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        txtPagoAdicional = new javax.swing.JTextField();
        txtPagoImpuesto = new javax.swing.JTextField();
        txtPagoTotal = new javax.swing.JTextField();
        btmLimpiar = new javax.swing.JButton();
        btmCancelar = new javax.swing.JButton();
        btmCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("Clave Contrato");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 20, 84, 16);

        jLabel2.setText("Num Empleado");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(40, 70, 86, 16);

        jLabel3.setText("Nombre");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 120, 45, 16);

        jLabel4.setText("Domicilio");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(40, 170, 51, 30);

        jLabel5.setText("Puesto");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(40, 230, 38, 16);

        jLabel6.setText("%Impuesto");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(40, 280, 65, 16);

        jLabel7.setText("Nivel");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(40, 320, 27, 16);

        jLabel8.setText("pago por hora");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(40, 370, 81, 16);

        jLabel9.setText("Horas trabajdas");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(40, 420, 91, 16);

        txtClaveContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClaveContratoActionPerformed(evt);
            }
        });
        getContentPane().add(txtClaveContrato);
        txtClaveContrato.setBounds(130, 20, 120, 22);
        getContentPane().add(txtNumEmpleado);
        txtNumEmpleado.setBounds(130, 70, 80, 22);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(90, 120, 160, 22);
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(100, 170, 170, 30);
        getContentPane().add(txtPuesto);
        txtPuesto.setBounds(90, 230, 100, 22);
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(110, 280, 100, 22);
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(130, 370, 110, 22);
        getContentPane().add(txtHorasTrabajadas);
        txtHorasTrabajadas.setBounds(140, 420, 100, 22);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pree escolar", "Primaria", "Secundaria" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(80, 320, 100, 22);

        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(730, 50, 67, 25);

        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(730, 110, 79, 25);

        btmMostrar.setText("Mostrar");
        btmMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(730, 160, 77, 25);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);

        jLabel10.setText("Calculos");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(40, 20, 47, 16);

        jLabel11.setText("Total:");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(200, 50, 40, 16);

        jLabel12.setText("Pago Adicional: (+)");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(200, 80, 110, 16);

        jLabel13.setText("Pago Impuesto (-)");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(200, 120, 110, 16);

        jLabel14.setText("Pago Total");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(200, 160, 70, 16);
        jPanel2.add(txtTotal);
        txtTotal.setBounds(240, 50, 140, 22);
        jPanel2.add(txtPagoAdicional);
        txtPagoAdicional.setBounds(310, 80, 120, 22);
        jPanel2.add(txtPagoImpuesto);
        txtPagoImpuesto.setBounds(310, 120, 120, 22);
        jPanel2.add(txtPagoTotal);
        txtPagoTotal.setBounds(270, 160, 160, 22);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(380, 270, 460, 250);

        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(250, 590, 75, 25);

        btmCancelar.setText("Cancelar");
        btmCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCancelar);
        btmCancelar.setBounds(360, 590, 83, 25);

        btmCerrar.setText("Cerrar");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(470, 590, 69, 25);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtClaveContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClaveContratoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClaveContratoActionPerformed

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        contratos = new Contrato();
        this.habilitar();

    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
       boolean exito = true;

    // Validar campos de texto
    if (txtClaveContrato.getText().isEmpty()) exito = false;
    if (txtNumEmpleado.getText().isEmpty()) exito = false;
    if (txtNombre.getText().isEmpty()) exito = false;
    if (txtDomicilio.getText().isEmpty()) exito = false;
    if (txtPuesto.getText().isEmpty()) exito = false;
    if (txtImpuesto.getText().isEmpty()) exito = false;
    if (cmbNivel.getSelectedIndex() == -1) exito = false;
    if (txtPagoHora.getText().isEmpty()) exito = false;
    if (txtHorasTrabajadas.getText().isEmpty()) exito = false;

    if (exito) {
        try {
            int claveContrato = Integer.parseInt(txtClaveContrato.getText());
            int numEmpleado = Integer.parseInt(txtNumEmpleado.getText());
            float impuestoISR = Float.parseFloat(txtImpuesto.getText());
            float pagoHora = Float.parseFloat(txtPagoHora.getText());
            float horasTrabajadas = Float.parseFloat(txtHorasTrabajadas.getText());
            int nivel = cmbNivel.getSelectedIndex() + 1; 

           
            contrato = new Contrato(claveContrato, txtPuesto.getText(), impuestoISR);
            docente = new EmpleadoDocente(nivel, horasTrabajadas, pagoHora, numEmpleado, txtNombre.getText(), txtDomicilio.getText(), contrato);

            JOptionPane.showMessageDialog(this, "Se guardó con éxito");
            btmMostrar.setEnabled(true);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Por favor, ingrese valores numéricos válidos en los campos correspondientes.", "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Ocurrió un error al guardar los datos: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    } else {
        JOptionPane.showMessageDialog(this, "Por favor, llene todos los campos.", "Error", JOptionPane.ERROR_MESSAGE);
    }
    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmMostrarActionPerformed
        // TODO add your handling code here:
   if (docente != null) {
            txtClaveContrato.setText(String.valueOf(contrato.getClaveContrato()));
            txtNumEmpleado.setText(String.valueOf(docente.getNumEmpleado()));
            txtNombre.setText(docente.getNombre());
            txtDomicilio.setText(docente.getDomicilio());
            txtPuesto.setText(contrato.getPuesto());
            txtImpuesto.setText(String.valueOf(contrato.getImpuestoISR()));
            cmbNivel.setSelectedIndex(docente.getNivel() - 1);
            txtPagoHora.setText(String.valueOf(docente.getPagoHoras()));
            txtHorasTrabajadas.setText(String.valueOf(docente.getHorasLaborales()));

            txtTotal.setText(String.valueOf(docente.calcularTotal()));
            txtPagoAdicional.setText(String.valueOf(docente.calcularTotal() - (docente.getHorasLaborales() * docente.getPagoHoras())));
            txtPagoImpuesto.setText(String.valueOf(docente.calcularImpuesto()));
            txtPagoTotal.setText(String.valueOf(docente.calcularTotal() - docente.calcularImpuesto()));
        } else {
            JOptionPane.showMessageDialog(this, "No hay datos para mostrar.");
        }
    }//GEN-LAST:event_btmMostrarActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.txtClaveContrato.setText("");
        this.txtNumEmpleado.setText("");
        this.txtNombre.setText("");
        this.txtDomicilio.setText("");
        this.txtPuesto.setText("");
        this.txtImpuesto.setText("");
        this.cmbNivel.setSelectedIndex(0);
        this.txtPagoHora.setText("");
        this.txtHorasTrabajadas.setText("");
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCancelarActionPerformed
        // TODO add your handling code here:

        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btmCancelarActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir",
            "Contratos", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();

        }
    }//GEN-LAST:event_btmCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jintContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jintContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jintContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jintContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jintContrato dialog = new jintContrato(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCancelar;
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmMostrar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtClaveContrato;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtHorasTrabajadas;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmpleado;
    private javax.swing.JTextField txtPagoAdicional;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoImpuesto;
    private javax.swing.JTextField txtPagoTotal;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
private Contrato contratos = new Contrato();


}
